var express 	= require('express');
var http 		= require('http');
var request 	= require('request');
var request_agent = require('superagent');
var app 		= express();
app.set('port', process.env.PORT || 3000);
var server 		= http.createServer(app);
var io 			= require('socket.io').listen(server);
var date_utils 	= require('date-utils');
server.listen(app.get('port'));		

app.configure(function(){ app.use(express.static(__dirname + '/public')); });
app.get('/', function(request, response){ response.sendfile(__dirname + "/index.html"); });

io.sockets.on('connection', function(client){

	url_webse = "http://52.0.8.85:9010/information.json?dia_semana=SEGUNDA-FEIRA"; //"http://localhost:3001/stores"; 
	client.on('OpenSearch', function(){
		request({ url: url_webse, json: true }, function(error, response, body){ if (!error && response.statusCode == 200) client.broadcast.emit('fetchAllIds', body); });		
	});

	url_webse_response = "http://52.0.8.85:8022/status_pdv.json"; //"http://localhost:3001/statuses";
	client.on('PassAllIds', function (data) {

		for (line in data) { 
			var cod = data[line]["cod_loja"];	
			var hora = data[line]["hora_abertura"]; 
			
			var callthis;
			callthis = function(cod) {

				request_agent.get(url_webse_response)
					.query({ loja: cod })
					.query({ data: "2013-06-03" })
					.set('Accept', 'application/json')
					.end(function(error, res){
						if (res["body"]["status"] === false) client.broadcast.emit('fetchSuccess', { status: res["body"]["status"], hora: hora, cod: cod }); 
				});
			}
			callthis(cod);			
		}
	});

});

console.log('scope open');