
$(document).ready(function(){ 
	window.div_output 	= $("div#types"); 
	window.p_count 		= $("p#counttypes");
});

var server = io.connect('http://localhost:3000');

server.on('connect', function (data){
	console.log('connect call');	
	window.div_output.empty();
	window.p_count.empty();
	server.emit('OpenSearch');
});

server.on('fetchAllIds', function (data){
	//console.log('fetchAllIds call', data);	
	server.emit('PassAllIds', data);
});

server.on('fetchSuccess', function (data){
	//console.log('fetchSuccess call', data);
	window.div_output.append("<i>status - "+data["status"]+" | Cod - "+data["cod"]+"</i><br />"); 
	window.p_count.text($("div#types i").length);
});